Senior Backend Engineer - Tech Test. Provided by IGS.
Solution by Jaroslaw Pola

REQUIREMENTS:

1. Visual Studio
2. SQL Server
3. Internet Access

INSTRUCTIOINS:

1. Clone or download this repository unto your local hard drive
2. Run Application.sln
3. Wait until Visual Studio finishes downloading dependencies
4. In "Solution Explorer" window, click on project solution and choose "Restore NuGet Packages"
5. Click on "Solution Explorer" again and choose "Build Solution"
6. In main menu's search bar type "Package Manager" and choose "Package Manager Console"
7. Select Package Manager from the console at the bottom of the screen and choose default application "Application.Dal
8. In the Package Manager Console type "update-database" and press enter. After a moment, app's database will be initialized.
9. You can now run the API app with IIS Server.
